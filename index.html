<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Robin Chan - Intro Keypoint-based 6D Pose Estimation</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/techfak.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section data-auto-animate>
					<h3 style="display: inline-block">Generative Modeling of Object Keypoints<br>for 6D Pose Estimation</h3>
					<h2 data-id="6d-pose"></h2>

					<div class="row">
						<img width=45% src="images/predictions_keypoints.png">
						<img width=45% src="images/prediction_pose_render.png">
					</div>

					<b>Robin Chan</b><br>
					<small><b><a style="color: #558CA0" href= "mailto:rchan@techfak.uni-bielefeld.de">rchan@techfak.uni-bielefeld.de</a></b></small>
					<br><br>
					<small>June 26, 2024, DataNinja sAIOnARA 2024 Conference, Bielefeld
					</small>
				</section>

				<section data-transition="fade-out" data-auto-animate>
					<section>
						<h3>What is 6D(oF) Pose Estimation?</h3>
						<ul>
							<small>
								<li class="fragment"><b style="color: #558CA0">Vision task:</b> estimate 3D translation and 3D rotation of objects</li>
								<li class="fragment"><b style="color: #558CA0">Input:</b> monocular RGB images and CAD models of objects of interest</li>
							</small>
						</ul>
						<br><br>
						<video data-autoplay loop src="videos/ycbv_tracking_c.mp4"></video>
					</section>
					<section>
						<h3>Possible Applications</h3>
						<ul>
							<small>
								<li>augmented reality, autonomous vehicles, robotics, ...</li>
								<li>useful when rich positional information is required</li>
							</small>
						</ul>
						<br><br>
						<video data-autoplay loop src="videos/robot_mustard.mp4"></video>
					</section>
				</section>

				<section>
					<div style="margin:0px auto; width: 60%;">
						<ul>
							<li><h4>General 3D Rotation:</h4></li>
						</ul>
						$$~~~~~\mathbf{R} = \mathbf{R}_z(\alpha)\ \mathbf{R}_y(\beta)\ \mathbf{R}_x(\gamma)$$
						<div class="fragment">
							<ul>
								<li><h4>Example 3D Rotation:</h4></li>
							</ul>
							<div class="row">
								<img width=46% style="margin-left: .8em; margin-top: -50px;" src="images/cat_cropped.webp">
								<img width=46% src="images/cat_rotated_cropped.webp">
								$\mathbf{R} \!=~\!\!\! \begin{pmatrix} 0.11\!\! & -0.92\!\! & 0.37\! \\ -0.69\!\! & -0.39\!\! & -0.62\! \\ 0.72\!\! & -0.19\!\! & -0.67\!\end{pmatrix}$
							</div>	
						</div>
					</div>
				</section>

				<section>
					<h3>Geometry-based Pose Estimation</h3>
					<div style="float: left; width: 50%;" class="fragment">
						<img width="90%" src="images/6d_pose_geometry.png">
						<div style="font-size: 0.6em;">
							<b style="color: #558CA0;">Given:</b><br>
							<ul>
								<li>2D-3D points: $\{(\mathbf{v}_i^\mathrm{2D}, \mathbf{v}_i^\mathrm{3D})\}_{i=1}^n$</li>
								<li>focal length $f \in \mathbb{R}$</li>
							</ul>
							<br><br>
							<b style="color: #558CA0;">We can calculate:</b><br>
							<ul>
								<li>rotation $\mathbf{R} \in \mathbb{R}^{3\times 3}$</li>
								<li>translation $\mathbf{t} \in \mathbb{R}^3$</li>
							</ul>
						</div>
					</div>
					<div style="float: right; width: 50%; font-size: 0.6em;" class="fragment">
						All points are given in their coordinate systems.<br>How to project 3D object points $\mathbf{v}^\mathrm{3D} = (U, V, W)^\top$ to 2D image points $\mathbf{v}^\mathrm{2D} = (x,y)^\top$?<br><br>
						<b style="color: #558CA0;">Transform: object -> camera:</b><br>
						$$\begin{pmatrix} X \\ Y \\ Z\end{pmatrix} = \mathbf{R} \begin{pmatrix}U \\ V \\ W\end{pmatrix} + \mathbf{t}$$
						<b style="color: #558CA0;">Transform: camera -> image:</b><br>
						$$x=f\frac{X}{Z},~~ y=f\frac{Y}{Z}$$
						<img width="48%" src="images/perspective_triangle_x.png">
						<img width="48%" src="images/perspective_triangle_y.png">
					</div>
				</section>

				
				<section>
					<h3>Keypoint-based 6D Pose Estimation</h3>
					<div style="margin:0px auto; width: 50%; font-size: 0.6em;" class="fragment" data-fragment-index="1">
						As the 2D points are usually not known, we need to estimate their locations, e.g. with a neural network.
						<div class="r-stack">
							<img width="90%" src="images/pipe.png" class="fragment fade-out" data-fragment-index="2">
							<img width="90%" src="images/pipe_emph.png" class="fragment current-visible" data-fragment-index="2">
							<img width="90%" src="images/pipe_emph.png" class="fragment" data-fragment-index="2">
						</div>
						<br>
						<div class="fragment" data-fragment-index="2">
							<b style="color: #558CA0;">Loss function / optimization objective:</b><br>
							$$\mathcal{L} = \frac{1}{2} \sum_{i=1}^n \left\| \begin{pmatrix} x_i \\ y_i\end{pmatrix} - \begin{pmatrix} \hat{x}_i \\ \hat{y}_i \end{pmatrix} \right\|_2^2$$
							<b style="color: #558CA0;">-> minimize the reprojection error</b>
						</div>
					</div>
				</section>

				<section>
					<h3>Prediction Examples</h3>
					<div class="r-stack">
						<div class="fragment" data-fragment-index="1"><img width="50%" src="images/3295_kpts.png"><img width="50%" src="images/3295_bbox.png"></div>
						<div class="fragment" data-fragment-index="2"><img width="50%" src="images/4769_kpts.png"><img width="50%" src="images/4769_bbox.png"></div>
						<div class="fragment" data-fragment-index="3"><img width="50%" src="images/22824_kpts.png"><img width="50%" src="images/22824_bbox.png"></div>
					</div>
				</section>

				<section>
					<h3>Failure Cases</h3>
					<img src="images/rand_keypoint_fail.png">
					<img src="images/rand_bbox_fail.png">
					<img src="images/rand_dist_keypoint.png" class="fragment" data-fragment-index="2">
					<br><b style="font-size: 0.8em; color: #558CA0;" class="fragment" data-fragment-index="2">Could we have identified such error...</b>
					<ul style="font-size: 0.8em;">
						<li class="fragment" data-fragment-index="2">... if we had predicted entire <b>sets of possible keypoints</b>? </li>
						<li class="fragment" data-fragment-index="2">... if we had information on the <b>spatial correlation of keypoints?</b></li>
					</ul>
				</section>

				<section data-auto-animate>
					<h3>Distribution of valid 2D keypoints sets</h3>
					<img width="90%" src="images/dist.png">
				</section>

				<section data-auto-animate>
					<h3>Distribution of valid 2D keypoints sets</h3>
					<b style="font-size: 0.65em;">"Ground Truth":</b><br>
					<img width="45%" src="images/dist.png">
				</section>

				<section data-auto-animate data-transition="fade">
					<h3>Distribution of valid 2D keypoints sets</h3>
					<div style="float: left; width: 50%; font-size: 0.65em;">
						<b>"Ground Truth":</b>
					</div>
					<div style="float: right; width: 50%; font-size: 0.65em;">
						<b>Prediction (Probabilistic) Generative Model:</b>
					</div>
					<img width="45%" src="images/dist.png"> &nbsp;&nbsp;&nbsp;&nbsp;
					<img width="45%" src="images/dist_pred_inn.png">
				</section>
				
				<section>
					<section>
						<h4>Uncertainty Quantification via Density Estimation</h4>
						<div class="row fragment" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/can_patch_kpts_err_16.3790.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/can_patch_bbox_nll_1578100.0000.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= 1578100.00 \\
									\textrm{err} &= 16.37
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row fragment" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/can_patch_kpts_err_6.8337.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/can_patch_bbox_nll_1376.2972.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= 1376.29 \\
									\textrm{err} &= 6.83
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row fragment" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/can_patch_kpts_err_3.2495.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/can_patch_bbox_nll_-50.1419.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= -50.14 \\
									\textrm{err} &= 3.24
									\end{align*}$$
								</b>
							</div>
						</div>
					</section>
					<section>
						<h4>Uncertainty Quantification via Density Estimation</h4>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/cat_patch_kpts_err_8.6165.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/cat_patch_bbox_nll_458.2758.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= 458.27 \\
									\textrm{err} &= 8.61
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/cat_patch_kpts_err_4.3665.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/cat_patch_bbox_nll_-37.4078.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= -37.40 \\
									\textrm{err} &= 4.36
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/cat_patch_kpts_err_1.9598.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/cat_patch_bbox_nll_-47.5524.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= -47.55 \\
									\textrm{err} &= 1.95
									\end{align*}$$
								</b>
							</div>
						</div>
					</section>
					<section>
						<h4>Uncertainty Quantification via Density Estimation</h4>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/duck_patch_kpts_err_8.1332.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/duck_patch_bbox_nll_5357.2842.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= 5357.28 \\
									\textrm{err} &= 8.13
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/duck_patch_kpts_err_4.7112.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/duck_patch_bbox_nll_-45.4301.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= -45.43 \\
									\textrm{err} &= 4.71
									\end{align*}$$
								</b>
							</div>
						</div>
						<div class="row" style="display: flex; align-items: center;">
							<img width="15%" src="images/eval/duck_patch_kpts_err_2.2493.png" style="margin-left: 20%;margin-right: 10px;">
							<img width="15%" src="images/eval/duck_patch_bbox_nll_-47.3016.png" style="margin-right: 20px;">
							<div>
								<b style="font-size: 0.8em; color: #558CA0; display: block;">
									$$\begin{align*} 
									\textrm{nll} &= -47.30 \\
									\textrm{err} &= 2.24
									\end{align*}$$
								</b>
							</div>
						</div>
					</section>
					<section>
						<div class="row">
							<h4 style="margin-bottom: 0;">Correlation between Likelihood and Error</h4>
							<img width="80%" src="images/eval/plot_correlation.png" style="margin-top: 5px;">
						</div>
					</section>
				</section>

				<section>
					<h3>Thank you.</h3>
				</section>

				<section data-transition="fade-out">
					<section data-transition="fade-out">
						<h3>What are Comuter-aided Design (CAD) Models?</h3>
						<iframe data-src="html/beethoven.html" width="1200" height="600"></iframe>
					</section>
					<section data-transition="fade-in">
						<h3>What are Comuter-aided Design (CAD) Models?</h3>
						<iframe data-src="html/beethoven_w_axes.html" width="1200" height="600"></iframe>
					</section>
					<section>
						<h3>How to Generate CAD Models?</h3>
						<div style="float: left; width: 40%;">
							<div class="row" style="display: flex; align-items: center;">
								<img style="margin-left: 20%" width="40%" src="images/cad/image.webp"><img width="40%" src="images/cad/image-1.webp">
							</div>
							<div class="row" style="display: flex; align-items: center;">
								<img style="margin-left: 20%" width="40%" src="images/cad/image-2.webp"><img width="40%" src="images/cad/image-3.webp">
							</div>
							<div class="row" style="display: flex; align-items: center;">
								<img style="margin-left: 20%" width="40%" src="images/cad/image-4.webp"><img width="40%" src="images/cad/image-5.webp">
							</div>
						</div>
						<div style="float: right; width: 60%; font-size: 0.55em;">
							<br><br>
							<b style="color: #558CA0">CAD Model generated with 3 images</b>
							<br>
							<b style="color: #558CA0">from 3 different views of the object:</b>
							<br><br>
							<video data-autoplay loop src="videos/lego_model.mp4"></video>
						</div>
					</section>
				</section>
				
				<section>
					<h3>How to represent Rotation?</h3>
					<ul style="width: 100%;">
						<li><b style="color: #558CA0">2D rotation matrix:</b>$~~\mathbf{R} = \begin{pmatrix} \cos\theta & -\sin\theta \\ \sin\theta & \cos\theta \end{pmatrix}, \theta \in [0^\circ,360^\circ]$</li>
						<li><b style="color: #558CA0">Rotate $v = \begin{pmatrix} x \\ y\end{pmatrix}$:</b>$~~\mathbf{R}v = \begin{pmatrix} x\cos\theta - y\sin\theta \\ x\sin\theta + y\cos\theta \end{pmatrix} = v^\prime \in\mathbb{R}^2$</li>
					</ul>
					<ul style="width: 76%;">
						<li><b style="color: #558CA0">Examples:</b>$ \begin{pmatrix}-1 & 0 \\ 0 & -1\end{pmatrix}\!v~$ rotates $v$ by $180^\circ$ <br>
							$~~~~~~~~~\begin{pmatrix}\sqrt{3}/2 & -1/2 \\ 1/2 & \sqrt{3}/2\end{pmatrix}\!v~$ rotates $v$ by $30^\circ$</li>
					</ul>
					<img width=20% style="float: right" src="images/clipboard_e57eb4496dc7d261594592a452b9f3a99.png">
				</section>

				<section>
					<div style="float: left; width: 50%;">
						<ul>
							<li><h4>3D Rotations about one axis:</h4></li>
						</ul>
						$$\mathbf{R}_x(\theta) \!=\!
						\begin{pmatrix}
						1 & 0 & 0 \\
						0 & \cos\theta & -\sin\theta \\ 
						0 & \sin\theta & \cos\theta
						\end{pmatrix} \\
					
					\mathbf{R}_y(\theta) \!=\!\!
						\begin{pmatrix}
						\cos\theta & 0 & \sin\theta \\
						0 & 1 & 0  \\ 
						-\sin\theta & 0 & \cos\theta
						\end{pmatrix} \\
					
					\mathbf{R}_z(\theta) \!=\!
						\begin{pmatrix}
						\cos\theta & -\sin\theta & 0 \\ 
						\sin\theta & \cos\theta & 0 \\
						0 & 0 & 1
						\end{pmatrix}$$
					</div>
					<div style="float: right; width: 50%;">
						<ul>
							<li><h4>General 3D Rotation:</h4></li>
						</ul>
						$$~~~~~\mathbf{R} = \mathbf{R}_z(\alpha)\ \mathbf{R}_y(\beta)\ \mathbf{R}_x(\gamma)$$
						<div>
							<ul>
								<li><h4>Example 3D Rotation:</h4></li>
							</ul>
							<div class="row">
								<img width=46% style="margin-left: .8em; margin-top: -50px;" src="images/cat_cropped.webp">
								<img width=46% src="images/cat_rotated_cropped.webp">
								$\mathbf{R} \!=~\!\!\! \begin{pmatrix} 0.11\!\! & -0.92\!\! & 0.37\! \\ -0.69\!\! & -0.39\!\! & -0.62\! \\ 0.72\!\! & -0.19\!\! & -0.67\!\end{pmatrix}$
							</div>	
						</div>
					</div>
				</section>

				<section>
					<h3>What are 2D-3D Point Correspondences?</h3>
					<iframe data-src="html/point_correspondences.html" width="1200" height="600"></iframe>
				</section>

				<section>
					<h3>How to estimate 6D Pose with 2D Keypoints?</h3>
					<div style="float: left; width: 50%; font-size: 0.55em;">
						<b style="color: #558CA0;">Projection Equations:</b><br>
						<div class="r-stack">
							<div>
								$$\begin{align*}
								\begin{pmatrix} x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! &= \mathbf{K}\mathbf{H} \begin{pmatrix} \mathbf{v}^\mathrm{3D} \\ 1\end{pmatrix} \Leftrightarrow \begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! = \mathbf{K}\mathbf{H} \begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix} \Leftrightarrow \\
								\textcolor{black}{\begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime\end{pmatrix}\!} &= \textcolor{black}{\!\begin{pmatrix} f & 0 & 0 & 0 \\ 0 & f & 0 & 0 \\ 0 & 0 & 1 & 0 \end{pmatrix}\! \begin{pmatrix}r_{11} & r_{12} & r_{13} & t_1 \\ r_{21} & r_{22} & r_{23} & t_2 \\ r_{31} & r_{32} & r_{33} & t_3 \\ 0 & 0 & 0 & 1\end{pmatrix}\! \begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix}}
								\\
								\Rightarrow x &= \frac{x^\prime}{z^\prime},~~ y = \frac{y^\prime}{z^\prime},~~ \begin{pmatrix}x \\ y \end{pmatrix} = \mathbf{v}^\mathrm{2D}
								\end{align*}
								$$
							</div>
							<div>
								$$\begin{align*}
								\begin{pmatrix} x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! &= \mathbf{K}\mathbf{H} \begin{pmatrix} \textcolor{blue}{\mathbf{v}^\mathrm{3D}} \\ 1\end{pmatrix} \Leftrightarrow \begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! = \mathbf{K}\mathbf{H} \begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix} \Leftrightarrow \\
								\textcolor{blue}{\begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime\end{pmatrix}\!} &= \textcolor{blue}{\!\begin{pmatrix} f & 0 & 0 & 0 \\ 0 & f & 0 & 0 \\ 0 & 0 & 1 & 0 \end{pmatrix}\! \begin{pmatrix}r_{11} & r_{12} & r_{13} & t_1 \\ r_{21} & r_{22} & r_{23} & t_2 \\ r_{31} & r_{32} & r_{33} & t_3 \\ 0 & 0 & 0 & 1\end{pmatrix}\! \begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix}}
								\\
								\Rightarrow x &= \frac{x^\prime}{z^\prime},~~ y = \frac{y^\prime}{z^\prime},~~ \begin{pmatrix}x \\ y \end{pmatrix} = \textcolor{blue}{\mathbf{v}^\mathrm{2D}}
								\end{align*}
								$$
							</div>
							<div>
								$$\begin{align*}
								\begin{pmatrix} x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! &= \mathbf{K}\mathbf{H} \begin{pmatrix} \textcolor{blue}{\mathbf{v}^\mathrm{3D}} \\ 1\end{pmatrix} \Leftrightarrow \begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime \end{pmatrix}\! = \mathbf{K}\mathbf{H} \begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix} \Leftrightarrow \\
								\textcolor{black}{\begin{pmatrix}x^\prime \\ y^\prime \\ z^\prime\end{pmatrix}\!} &= \!\begin{pmatrix} f & 0 & 0 & 0 \\ 0 & f & 0 & 0 \\ 0 & 0 & 1 & 0 \end{pmatrix}\! \textcolor{red}{\begin{pmatrix}r_{11} & r_{12} & r_{13} & t_1 \\ r_{21} & r_{22} & r_{23} & t_2 \\ r_{31} & r_{32} & r_{33} & t_3 \\ 0 & 0 & 0 & 1\end{pmatrix}\!} \textcolor{black}{\begin{pmatrix}U \\ V \\ W \\ 1\end{pmatrix}}
								\\
								\Rightarrow x &= \frac{x^\prime}{z^\prime},~~ y = \frac{y^\prime}{z^\prime},~~ \begin{pmatrix}x \\ y \end{pmatrix} = \textcolor{blue}{\mathbf{v}^\mathrm{2D}}
								\end{align*}
								$$
							</div>
						</div>
						<div>Can be solved with Perspective-n-Point algorithms.</div>
					</div>
					<div style="float: right; width: 50%; font-size: 0.6em;">
						As the 2D points are usually not known, we need to estimate their locations, e.g. with a neural network.
						<div class="r-stack">
							<img width="90%" src="images/pipe.png">
							<img width="90%" src="images/pipe_emph.png">
							<img width="90%" src="images/pipe_emph.png">
						</div>
						<br>
						<div>
							<b style="color: #558CA0;">Loss function / optimization objective:</b><br>
							$$\mathcal{L} = \frac{1}{2} \sum_{i=1}^n \| \mathbf{v}_i^\mathrm{2D}-\hat{\mathbf{v}}_i^\mathrm{2D} \|_2^2 ~~ \text{or}$$
							$$\Leftrightarrow \mathcal{L} = \frac{1}{2} \sum_{i=1}^n \left\| \mathbf{K}\mathbf{H} \begin{pmatrix} \mathbf{v}^\mathrm{3D}_i \\ 1 \end{pmatrix} - \mathbf{K}\hat{\mathbf{H}}\begin{pmatrix} \mathbf{v}^\mathrm{3D}_i \\ 1 \end{pmatrix} \right\|_2^2$$
						</div>
					</div>
				</section>

			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script src="plugin/appearance/appearance.js"></script>
		<script src="plugin/zoom/zoom.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,
				// controlsLayout: 'edges',
				// slideNumber: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMarkdown, RevealHighlight, RevealNotes, Appearance, RevealZoom ]
			});
		</script>
		<script src="plugin/math/math.js"></script>
		<script>
			Reveal.initialize({ plugins: [ RevealMath.KaTeX, ] });
		</script>
	</body>
</html>
